const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const port = 3000; //porta padrão
const mysql = require("mysql");
const { text } = require("body-parser");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const router = express.Router();
app.use("/", router);

app.listen(port);
console.log("API rodando na porta " + port + "...");

router.get("/", (req, res) => res.json({ message: "Funcionando!" }));
router.get("/");

router.get("/status", (req, res) => {
  execSQLQuery("SELECT * FROM status", res);
});
router.post("/newStatus", (req, res) => {
  execSQLQuery(
    "INSERT INTO status (nome) values ('" + req.body.text + "')",
    res
  );
});

router.get("/cargos", (req, res) => {
  execSQLQuery("SELECT * FROM cargo", res);
});
router.post("/newCargo", (req, res) => {
  execSQLQuery(
    "INSERT INTO cargo (nome) values ('" + req.body.text + "')",
    res
  );
});

function execSQLQuery(sqlQry, res) {
  const connection = mysql.createConnection({
    host: "localhost",
    port: 3306,
    user: "gabriel",
    password: "123456",
    database: "fake",
  });

  connection.query(sqlQry, function (error, results, fields) {
    if (error) res.json(error);
    else res.json({ data: results, code: "200", message: "Sucesso" });
    connection.end();
    console.log("executou!");
  });
}
